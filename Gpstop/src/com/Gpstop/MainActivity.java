package com.Gpstop;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {
	
	private GpsPointsHelper pointsHelper;
	private LocationManager locService;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //check if GPS is connected
        locService = (LocationManager) getSystemService(LOCATION_SERVICE);
      
        if (!locService.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        	alertDialogBuilder.setTitle("GPS não ativo!");
        	alertDialogBuilder.setMessage("Tem de ativar os satélites GPS nos Serviços de localização para poder continuar")
        	
        	.setCancelable(false)
        	.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		        	startActivity(intent);
				}
			  })
			  .setNegativeButton("Sair da aplicação", new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog,int id) {
					  MainActivity.this.finish();
				}
			  });
        	
        	AlertDialog alertDialog = alertDialogBuilder.create();
        	alertDialog.show();
        } 
        
        pointsHelper = new GpsPointsHelper(getBaseContext());
       
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void markPoint(View view){
    	String provider = locService.getBestProvider(new Criteria(), false);
	    Location location = locService.getLastKnownLocation(provider);
	    double latit = location.getLatitude();
	    double longit = location.getLongitude();
	    writePoint(latit, longit);
    	
    }
    
    public void openMap(View view){
    	Intent i = new Intent(getBaseContext(), OpenMapActivity.class);                      
    	startActivity(i);
    }
    
    private void writePoint(double latitude, double longitude){
		GpsPointsHelper pointsHelper = new GpsPointsHelper(getBaseContext());
		SQLiteDatabase pointsDb = pointsHelper.getWritableDatabase();
		ContentValues point = new ContentValues();
		point.put("latitude", latitude);
		point.put("longitude", longitude);
		pointsDb.insert("gps_points", null, point);
		pointsDb.close();
	}
}

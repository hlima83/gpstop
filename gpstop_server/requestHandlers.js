var databaseUrl = "gps_points"; 
var collections = ["points"];
var db = require("mongojs").connect(databaseUrl, collections);

function addPoint(point){
	db.points.save({long: point.long, lat: point.lat}, function(err, saved) {
	//console.log(retMsg);
  	if( err || !saved ){ 
		console.log("Point not saved");
	}
  	else {
		console.log("Point " + point.long + ", " + point.lat + " saved");
	}
	});
	console.log(retMsg);
	return retMsg;
}

function seePoints(){
	var points;
	db.points.find({}, function(err, points) {
  	if( err || !points) console.log("No points found");
  	else {
		points.forEach( function(p) {
    			points.push(p);
  		});
	     return JSON.stringify(points);
	     }
	});
}

exports.addPoint = addPoint;
exports.seePoints = seePoints;

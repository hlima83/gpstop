var databaseUrl = "gps_points"; 
var collections = ["points"];
var db = require("mongojs").connect(databaseUrl, collections);

function getPoints(response){
	
	db.points.find({}, function(err,points){
		if(!err){
			var ret = {success: true, points: points};
			
		}
		else{
			var ret = {success: false};
		}
		response.writeHead(200, {"Content-Type": "text/plain"});
  		response.write(JSON.stringify(ret));
  		response.end();
	});
}

exports.getPoints = getPoints;

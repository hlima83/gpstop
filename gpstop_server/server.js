var http = require("http");
var url = require("url");

function start(route){
	function onRequest(request, response) {
  		var pathname = url.parse(request.url).pathname;
		var params = url.parse(request.url, true).query;
		route(pathname, params,response);
	}

http.createServer(onRequest).listen(8888);
console.log("Server has started.");

}

exports.start = start;

var databaseUrl = "gps_points"; 
var collections = ["points"];
var db = require("mongojs").connect(databaseUrl, collections);

function addPoint(response,point){
	db.points.save({long: point.long, lat: point.lat}, function(err, saved) {
	
  	if( err || !saved ){ 
		var ret = {success: false};
	}
  	else {
		var ret = {success: true, point: point} 
	}
	response.writeHead(200, {"Content-Type": "text/plain"});
  	response.write(JSON.stringify(ret));
  	response.end();
	});
	
}

exports.addPoint = addPoint;


var addPointService = require("./addPoint");
var getPointsService = require("./getPoints");

var webServices = {
	'/addPoint': addPointService.addPoint,
	'/getPoints': getPointsService.getPoints
};

function route(pathname, params, response) {
	console.log(pathname);
	var serv = webServices[pathname];
	serv(response,params);
	//console.log(serv(params));
}

exports.route = route;
